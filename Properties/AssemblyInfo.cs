using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: AssemblyCompany("My Company")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("My Copyright")]
[assembly: AssemblyDescription("Description Here")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("Assembly Details")]
[assembly: AssemblyTitle("Assembly Title")]
[assembly: AssemblyTrademark("Assembly Trademark")]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: ComVisible(false)]

